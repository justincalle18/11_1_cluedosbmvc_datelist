package ch.bbw.pr.cluedo.model;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * PunchListService
 *"
 * @author bbwpr
 * @version 29.08.2022
 */
@Service
public class DataService {
   private List<Person> persons = List.of(
         new Person("Mustard", "Colonel", "Yellow", 89),
         new Person("Scarlett", "Miss", "Red", 33),
         new Person("Plum", "Professor", "Violett", 56),
         new Person("Green", "Reverand", "Green", 49),
           new Person("Peacock", "Mrs.", "Blue", 41),
           new Person("White", "Mrs.", "White", 61)
   );

   private List<Weapon> weapons = List.of(
           new Weapon("pistol", 100, "Black", "far"),
           new Weapon("seil", 50, "Brown", "near"),
           new Weapon("rohr", 20, "Golden", "near"),
           new Weapon("messer", 69420, "Silver", "near"),
           new Weapon("ak", 500, "Gold", "far"),
           new Weapon("schotgun", 99999, "Red-Black", "middle")
   );

   public List<Person> getPersons() {
      return persons;
   }

   public List<Weapon> getWeapons() {
      return weapons;
   }
}
