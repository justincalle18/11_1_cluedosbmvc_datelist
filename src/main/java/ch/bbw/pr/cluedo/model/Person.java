package ch.bbw.pr.cluedo.model;

import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;

/**
 * A Person
 * @author Peter Rutschmann
 * @version 29.08.2022
 */
public class Person {
   private String name;
   private String formOfAdress;
   private String colour;
   private int age;

   public Person(String name, String formOfAdress, String colour, int age) {
      this.name = name;
      this.formOfAdress = formOfAdress;
      this.colour = colour;
      this.age = age;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getFormOfAdress() {
      return formOfAdress;
   }

   public void setFormOfAdress(String formOfAdress) {
      this.formOfAdress = formOfAdress;
   }

   public String getColour() {
      return colour;
   }

   public void setColour(String colour) {
      this.colour = colour;
   }

   public int getAge() {
      return age;
   }

   public void setAge(int age) {
      this.age = age;
   }

   @Override
   public String toString() {
      return "Person{" +
              "name='" + name + '\'' +
              ", formOfAdress='" + formOfAdress + '\'' +
              ", colour='" + colour + '\'' +
              ", age=" + age +
              '}';
   }
}

