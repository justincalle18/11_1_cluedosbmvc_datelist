package ch.bbw.pr.cluedo.model;

public class Weapon {

    private String name;
    private int damage;
    private String farbe;
    private String range;

    public Weapon(String name,int schaden, String farbe, String weite){
        this.name = name;
        this.farbe = farbe;
        this.damage = schaden;
        this.range = weite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int schaden) {
        this.damage = schaden;
    }

    public String getColour() {
        return farbe;
    }

    public void setColour(String farbe) {
        this.farbe = farbe;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }



    @Override
    public String toString() {
        return "Weapon{" +
                "name='" + name + '\'' +
                ", damage=" + damage +
                ", colour='" + farbe + '\'' +
                ", type='" + range + '\'' +
                '}';
    }
}
