package ch.bbw.pr.cluedo;

import ch.bbw.pr.cluedo.model.Crime;
import ch.bbw.pr.cluedo.model.GameLogic;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class CluedoApplicationTests {

	@Test
	void evaluateSuggestion() {
		Crime suggestion = new Crime();
		Crime secret = new Crime();
		int numberOfSuggestion = 0;
		int maxNumberOfSuggestions = 8;

		//setup secret
		secret.setActor(1);
		secret.setWeapon(1);
		secret.setScene(1);

		//setup suggestion with same values as secret
		//so I expact to win
		suggestion.setActor(1);
		suggestion.setWeapon(1);
		suggestion.setScene(1);

		GameLogic gameLogic = new GameLogic();

		//return true is expected
		assertTrue(gameLogic.evaluateSuggestion(suggestion, secret, numberOfSuggestion, maxNumberOfSuggestions));

	}


}
